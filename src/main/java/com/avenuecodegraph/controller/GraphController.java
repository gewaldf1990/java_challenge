package com.avenuecodegraph.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.avenuecodegraph.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.avenuecodegraph.entity.Graph;
import com.avenuecodegraph.service.GraphService;
import com.google.gson.Gson;

@RestController
@RequestMapping("/")
public class GraphController {

	public static int id;

	int numeroMaximoPassos;

	Routes routes;
	Map<String, Integer> routeDistance;

	@Autowired
	private GraphService GraphService;

	@RequestMapping(method = RequestMethod.GET)
	public Collection<Graph> getAllGraphs() {
		return GraphService.getAllGraphs();
	}

	// http://localhost:8080/Graphs/routes/from/1/to/2?maxStops=700
	@RequestMapping(value = "/routes/{graph_id}/from/{town_1}/to/{town_2}", method = RequestMethod.POST)
	public ResponseEntity routes(@PathVariable("town_1") String town_1, @PathVariable("town_2") String town_2,
			@PathVariable("graph_id") int id, @RequestParam(value = "maxStops", required = false) String maxStops) {

		
		// Recuperando o grafo...
		Graph grafo = GraphService.getGraphById(id - 1);

		Routes routes = constroiRotas(town_1, town_2, maxStops, grafo);
		Gson gson = new Gson();
		return ResponseEntity.ok(gson.toJson(routes));

	}

	public Routes constroiRotas(String town_1, String town_2, String maxStops, Graph grafo) {
		List<Data> lData = grafo.getData();
		List<String> lMascara = new ArrayList<>();
		int tamanho = 0;
		routeDistance = new HashMap<String, Integer>();

		
		for (int i = 0; i < lData.size(); i++) {
			String s = lData.get(i).getSource();
			String t = lData.get(i).getTarget();
			if (!(lMascara.contains(s))) {
				tamanho++;
				lMascara.add(s);
			
			}
			if (!(lMascara.contains(t))) {
				tamanho++;
				lMascara.add(t);
			}
		}
		if (maxStops != null) {
			numeroMaximoPassos = Integer.parseInt(maxStops);
		} else {
			numeroMaximoPassos = -1;
		}

		// Construindo matriz de adjacencia...
		// inicializando a matriz
		ArrayList<ArrayList<Integer>> matriz = new ArrayList<ArrayList<Integer>>();
		for (int i = 0; i < tamanho; i++) {

			List<Integer> linha = new ArrayList<>();
			for (int j = 0; j < tamanho; j++) {
				linha.add(0);
			}
			matriz.add((ArrayList) linha);

		}

		for (int i = 0; i < lData.size(); i++) {
			String s = lData.get(i).getSource();
			String t = lData.get(i).getTarget();
			int distancia = lData.get(i).getDistance();
			matriz.get(lMascara.indexOf(s)).set(lMascara.indexOf(t), distancia);

		}

		int s = lMascara.indexOf(town_1);
		int t = lMascara.indexOf(town_2);
		int count = -1;

		routes = new Routes();
		pegarTodosCaminhos(s, t, tamanho, matriz, lMascara, numeroMaximoPassos);
		return routes;

	}

	public void pegarTodosCaminhos(int s, int d, int tamanho, ArrayList<ArrayList<Integer>> matriz, List<String> lMascara,
			int numeroSaltos) {

		int distancia = 0;
		boolean[] visited = new boolean[tamanho];
		int[] path = new int[tamanho];
		int path_index = 0;
		for (int i = 0; i < tamanho; i++) {
			visited[i] = false;
		}
		List<Route> minhasRotas = new ArrayList<>();

		routes = new Routes(minhasRotas);

		pegarTodosCaminhosUtil(s, d, visited, path, path_index, matriz, lMascara, distancia, numeroSaltos);
		
	}

	@RequestMapping(value = "/routes/from/{town_1}/to/{town_2}", method = RequestMethod.POST)
	public ResponseEntity rotas(@PathVariable("town_1") String town_1, @PathVariable("town_2") String town_2,
			@RequestParam(value = "maxStops", required = false) String maxStops, @RequestBody Graph grafo) {

		// Recuperando o grafo...
		List<Data> lData = grafo.getData();
		List<String> lMascara = new ArrayList<>();
		int tamanho = 0;
		for (int i = 0; i < lData.size(); i++) {
			String s = lData.get(i).getSource();
			String t = lData.get(i).getTarget();
			if (!(lMascara.contains(s))) {
				tamanho++;
				lMascara.add(s);
		
			}
			if (!(lMascara.contains(t))) {
				tamanho++;
				lMascara.add(t);
				
			}
		}
		if (maxStops != null) {
			numeroMaximoPassos = Integer.parseInt(maxStops);
		} else {
			numeroMaximoPassos = -1;
		}

		// Construindo matriz de adjacencia...
		// inicializando a matriz
		ArrayList<ArrayList<Integer>> matriz = new ArrayList<ArrayList<Integer>>();
		for (int i = 0; i < tamanho; i++) {

			List<Integer> linha = new ArrayList<>();
			for (int j = 0; j < tamanho; j++) {
				linha.add(0);
			}
			matriz.add((ArrayList) linha);

		}

		for (int i = 0; i < lData.size(); i++) {
			String s = lData.get(i).getSource();
			String t = lData.get(i).getTarget();
			int distancia = lData.get(i).getDistance();
			matriz.get(lMascara.indexOf(s)).set(lMascara.indexOf(t), distancia);

		}

		int s = lMascara.indexOf(town_1);
		int t = lMascara.indexOf(town_2);

		routes = new Routes();
		pegarTodosCaminhos(s, t, tamanho, matriz, lMascara, numeroMaximoPassos);
		Gson gson = new Gson();

		return ResponseEntity.ok(gson.toJson(routes));

	}

	public void pegarTodosCaminhosUtil(int u, int d, boolean[] visited, int[] path, int path_index,
			ArrayList<ArrayList<Integer>> matriz, List<String> lMascara, int distancia, int numeroSaltos) {
		visited[u] = true;
		path[path_index] = u;
		path_index++;

		if (u == d) {
			if (numeroSaltos != -1) {
				if (numeroSaltos >= path_index) {
					Route route = new Route();
					String rota = "";

					for (int i = 0; i < path_index; i++) {
					
						rota += lMascara.get(path[i]);

					}
					int y = 0;
					int myDistance = 0;
					for (int x = 0; x < path_index - 1; x++) {
						y = x + 1;
						myDistance += matriz.get(path[x]).get(path[y]);

					}

					routeDistance.put(rota, myDistance);
					route.setRoute(rota);
					route.setStops(path_index - 1);
					routes.getRoutes().add(route);

				}
			} else {
				Route route = new Route();
				String rota = "";
				for (int i = 0; i < path_index; i++) {
					
					rota += lMascara.get(path[i]);
				}

				int y = 0;
				int myDistance = 0;
				for (int x = 0; x < path_index - 1; x++) {
					y = x + 1;
					myDistance += matriz.get(path[x]).get(path[y]);

				}

				routeDistance.put(rota, myDistance);
				route.setRoute(rota);
				route.setStops(path_index - 1);
				routes.getRoutes().add(route);
			}

			distancia = 0;
		} else {
			for (int i = 0; i < matriz.get(u).size(); i++) {
				if (!visited[i] && matriz.get(u).get(i) != 0) {
					distancia += matriz.get(u).get(i);

					pegarTodosCaminhosUtil(i, d, visited, path, path_index, matriz, lMascara, distancia, numeroSaltos);
				}
			}
		}

		path_index--;
		visited[u] = false;
	}

	// http://<host>:<port>/distance
	@RequestMapping(value = "/distance", method = RequestMethod.POST)
	public ResponseEntity distancia(@RequestBody GraphPath graphPath) {

		String pontoA = graphPath.getPath().get(0);

		String pontoB = graphPath.getPath().get(graphPath.getPath().size() - 1);

		Graph grafo = new Graph();
		grafo.setData(graphPath.getData());

		Routes routes = constroiRotas(pontoA, pontoB, String.valueOf(graphPath.getPath().size()), grafo);

		if (routes == null || routes.getRoutes().size() == 0) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}
		String distancia = "";
		for (int i = 0; i < routes.getRoutes().size(); i++) {

			if (routes.getRoutes().get(i).getRoute().equals(String.join("", graphPath.getPath()))) {
				// distance = routes.getRoutes().get(i).getRoute();
				distancia = String.valueOf(routeDistance.get(routes.getRoutes().get(i).getRoute()));
			}
		}
		// routes.getRoutes().get(index);

		Distance distance = new Distance(distancia);
		Gson gson = new Gson();
		return ResponseEntity.ok(gson.toJson(distance));

	}

	// http://<host>:<port>/distance/<graph id>`
	@RequestMapping(value = "/distance/{graph_id}", method = RequestMethod.POST)
	public ResponseEntity distanceByGraph(@PathVariable("graph_id") int id, @RequestBody Path path) {

		// Recuperando o grafo...
		Graph grafo = GraphService.getGraphById(id - 1);

		String pontoA = path.getPath().get(0);

		String pontoB = path.getPath().get(path.getPath().size() - 1);

		Routes routes = constroiRotas(pontoA, pontoB, String.valueOf(path.getPath().size()), grafo);

		if (routes == null || routes.getRoutes().size() == 0) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}
		String distancia = "";
		for (int i = 0; i < routes.getRoutes().size(); i++) {

			if (routes.getRoutes().get(i).getRoute().equals(String.join("", path.getPath()))) {
				// distance = routes.getRoutes().get(i).getRoute();
				distancia = String.valueOf(routeDistance.get(routes.getRoutes().get(i).getRoute()));
			}
		}
		// routes.getRoutes().get(index);

		Distance distance = new Distance(distancia);
		Gson gson = new Gson();
		return ResponseEntity.ok(gson.toJson(distance));
	}

	// http://<host>:<port>/distance/from/<town 1>/to/<town 2>`
	@RequestMapping(value = "/distance/from/{town_1}/to/{town_2}", method = RequestMethod.POST)
	public ResponseEntity distanceByTwoTowns(@PathVariable("town_1") String town_1,
			@PathVariable("town_2") String town_2, @RequestBody Graph graph) {

		
		Routes routes = constroiRotas(town_1, town_2, null, graph);

		if (routes == null || routes.getRoutes().size() == 0) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}
		Integer menor = null;
		String path = "";
		Iterator it = routeDistance.entrySet().iterator();
		if (it.hasNext()) {
			Map.Entry pairs = (Map.Entry) it.next();
			menor = (Integer) pairs.getValue();
			path = (String) pairs.getKey();
			it.remove();
		}

		while (it.hasNext()) {
			Map.Entry pairs = (Map.Entry) it.next();
			if (menor > (Integer) pairs.getValue()) {
				menor = (Integer) pairs.getValue();
				path = (String) pairs.getKey();
			}
			it.remove();
		}

		Distance distancia = new Distance(menor.toString());
		ShortDistance shortDistance = new ShortDistance(new ArrayList<String>(Arrays.asList(path.split(""))),
				distancia);

		Gson gson = new Gson();
		return ResponseEntity.ok(gson.toJson(shortDistance));
		// return shortDistance;
	}

	// `http://<host>:<port>/distance/<graph id>/from/<town 1>/to/<town 2>`
	@RequestMapping(value = "/distance/{graph_id}/from/{town_1}/to/{town_2}", method = RequestMethod.POST)
	public ResponseEntity distanceByTwoTownsGraphId(@PathVariable("town_1") String town_1,
			@PathVariable("town_2") String town_2, @PathVariable("graph_id") int id) {

		// Recuperando o grafo...
		Graph grafo = GraphService.getGraphById(id - 1);

		Routes routes = constroiRotas(town_1, town_2, null, grafo);

		if (routes == null || routes.getRoutes().size() == 0) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}
		Integer menor = null;
		String path = "";
		Iterator it = routeDistance.entrySet().iterator();
		if (it.hasNext()) {
			Map.Entry pairs = (Map.Entry) it.next();
			menor = (Integer) pairs.getValue();
			path = (String) pairs.getKey();
			it.remove();
		}

		while (it.hasNext()) {
			Map.Entry pairs = (Map.Entry) it.next();
			if (menor > (Integer) pairs.getValue()) {
				menor = (Integer) pairs.getValue();
				path = (String) pairs.getKey();
			}
			it.remove();
		}

		Distance distancia = new Distance(menor.toString());
		ShortDistance shortDistance = new ShortDistance(new ArrayList<String>(Arrays.asList(path.split(""))),
				distancia);

		Gson gson = new Gson();

		return ResponseEntity.ok(gson.toJson(shortDistance));
	}

	@RequestMapping(value = "/graph/{id}", method = RequestMethod.GET)
	public ResponseEntity getGraphById(@PathVariable("id") int id) {
		Graph graph = GraphService.getGraphById(id - 1);
		Gson gson = new Gson();
		return ResponseEntity.ok(gson.toJson(graph));
	}

	@RequestMapping(value = "/graph", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity insertGraph(@RequestBody Graph graph) {
		graph.setId(++id);

		GraphService.insertGraph(graph);
		Gson gson = new Gson();

		return ResponseEntity.status(HttpStatus.CREATED).body(gson.toJson(graph));

	}
}
