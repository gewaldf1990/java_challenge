package com.avenuecodegraph.entity;

import java.util.List;

public class Path {

	private List<String> path;

	public Path() {}
	
	public Path(List<String> path) {
		this.path = path;
	}

	public List<String> getPath() {
		return path;
	}

	public void setPath(List<String> path) {
		this.path = path;
	}
	
}