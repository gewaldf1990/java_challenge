package com.avenuecodegraph.entity;

import java.util.ArrayList;
import java.util.List;

public class GraphPath {

	private List<Data> data = new ArrayList<>();
	private List<String> path = new ArrayList<>();
	
	public GraphPath(List<Data> data, List<String> path) {
		super();
		this.data = data;
		this.path = path;
	}
	
	public GraphPath() {}
	
	public List<Data> getData() {
		return data;
	}
	public void setData(List<Data> data) {
		this.data = data;
	}
	public List<String> getPath() {
		return path;
	}
	public void setPath(List<String> path) {
		this.path = path;
	}
	
	   
	   
	
	
}
