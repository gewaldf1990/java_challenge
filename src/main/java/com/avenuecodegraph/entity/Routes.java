package com.avenuecodegraph.entity;

import java.util.List;

public class Routes {

	private List<Route> routes;
	

	public Routes(List<Route> routes) {
		this.routes = routes;
	}
	
	public Routes() {}

	public List<Route> getRoutes() {
		return routes;
	}

	public void setRoutes(List<Route> routes) {
		this.routes = routes;
	}
	
	
	
}
