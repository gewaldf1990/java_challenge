package com.avenuecodegraph.entity;

public class Data {
	
	 private String source;
	 private String target;
	 private int distance;
	 
	public Data(String source, String target, int distance) {
		this.source = source;
		this.target = target;
		this.distance = distance;
	}
	
	public Data(){
		
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public int getDistance() {
		return distance;
	}
	public void setDistance(int distance) {
		this.distance = distance;
	}

	 
	 
}
