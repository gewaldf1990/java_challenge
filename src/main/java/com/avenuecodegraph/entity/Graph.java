package com.avenuecodegraph.entity;

import java.util.ArrayList;
import java.util.List;

public class Graph {

    private int id;
    private List<Data> data = new ArrayList<>();

    public Graph(int id) {
        this.id = id;
        this.data = new ArrayList<Data>();
    }

    public Graph(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

	public List<Data> getData() {
		return data;
	}

	public void setData(List<Data> data) {
		this.data = data;
	}

}