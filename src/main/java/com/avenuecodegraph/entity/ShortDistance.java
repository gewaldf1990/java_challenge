package com.avenuecodegraph.entity;

import java.util.List;

public class ShortDistance {

	private List<String> path;
	
	private Distance distance;
	
	public ShortDistance(List<String> path, Distance distance) {
		this.path = path;
		this.distance = distance;
	}

	public ShortDistance() {}
	
	public List<String> getPath() {
		return path;
	}

	public void setPath(List<String> path) {
		this.path = path;
	}


	public Distance getDistance() {
		return distance;
	}

	public void setDistance(Distance distance) {
		this.distance = distance;
	}
	
	
	
}
