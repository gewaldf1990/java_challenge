package com.avenuecodegraph.dao;

import java.util.Collection;
import java.util.List;

import com.avenuecodegraph.entity.Data;
import com.avenuecodegraph.entity.Graph;

public interface GraphDao {
    Collection<Graph> getAllGraphs();

    Graph getGraphById(int id);

    void removeGraphById(int id);

    void updateGraph(Graph Graph);

    void insertGraphToDb(Graph Graph);
}
