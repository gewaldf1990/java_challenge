package com.avenuecodegraph.dao;

import com.avenuecodegraph.entity.Graph;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class GraphDaoImpl implements GraphDao {

	private static ArrayList<Graph> Graphs = new ArrayList<>();

//    static {
//
//        Graphs = new HashMap<Integer, Graph>(){
//
//            {
//                put(1, new Graph(1, "Said", "Computer Science"));
//                put(2, new Graph(2, "Alex U", "Finance"));
//                put(3, new Graph(3, "Anna", "Maths"));
//            }
//        };
//    }
	
	
    @Override
    public Collection<Graph> getAllGraphs() {
//        return new ArrayList<Graph>(){
//            {
//                add(new Graph(1, "Mario", "Nothing"));
//                add(new Graph(2, "Mario", "Nothing"));
//                add(new Graph(3, "Mario", "Nothing"));
//                
//            }
//        };
    	return Graphs;
    }

    @Override
    public Graph getGraphById(int id) {
    	 return this.Graphs.get(id);
    }

    @Override
    public void removeGraphById(int id) {

    }

    @Override
    public void updateGraph(Graph Graph) {

    }

    @Override
    public void insertGraphToDb(Graph Graph) {
    	
    	this.Graphs.add(Graph);

    }
}