package com.avenuecodegraph.service;

import com.avenuecodegraph.dao.GraphDao;
import com.avenuecodegraph.entity.Graph;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class GraphService {

    @Autowired
    private GraphDao GraphDao;

    public Collection<Graph> getAllGraphs(){
        return this.GraphDao.getAllGraphs();
    }

    public Graph getGraphById(int id){
        return this.GraphDao.getGraphById(id);
    }

    public void removeGraphById(int id) {
        this.GraphDao.removeGraphById(id);
    }

    public void updateGraph(Graph Graph){
        this.GraphDao.updateGraph(Graph);
    }

    public void insertGraph(Graph Graph) {
        this.GraphDao.insertGraphToDb(Graph);
    }
}